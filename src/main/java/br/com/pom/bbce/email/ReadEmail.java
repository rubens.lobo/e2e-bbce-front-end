package br.com.pom.bbce.email;

import br.com.core.email.Email;
import org.testng.Assert;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import java.io.IOException;

public class ReadEmail {

    private Folder inboxFolder;
    private Email email;

    public ReadEmail(String userEmail, String userSenha) {
        email = new Email("imap.gmail.com","993",userEmail,userSenha);
        inboxFolder = email.getFolder(email.connect("imap"),"INBOX",true);
    }

    /**
     * Limpa caixa de entrada do email
     */
    public void limpaCaixaDeEmails(){
        try{
            Message[] emails = inboxFolder.getMessages();
            email.deleteMessage(emails);
        } catch (MessagingException e) {
            Assert.fail("Erro ao tentar limpar caixa de email!");
        }
    }

    /**
     *
     * Recupera senha incial no email para o processo de criação de usuário
     *
     * @return senha incial
     */
    public String recuperaEmailComSenhaInicial() {
        String bodyMessage;

        try {
            Message[] emails = inboxFolder.getMessages();

            for (int i = emails.length; i >= 0; i--) {
                Message message = emails[i -1];
                String subject = message.getSubject();
                if(subject.contains("Nova senha de acesso - BBCE")){
                    Object obj = message.getContent();
                    Multipart mp = (Multipart)obj;
                    mp.getBodyPart(0);
                    bodyMessage =  email.getTextFromBodyMessage(message);
                    return bodyMessage.substring(bodyMessage.indexOf("é:") + 2,bodyMessage.indexOf("ACESSA")).trim();
                }
            }
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * Recupera Token no email para o processo de criação de usuário
     *
     * @return senha incial
     */
    public String recuperaEmailComToken() {
        String bodyMessage;

        try {
            Message[] emails = inboxFolder.getMessages();

            for (int i = emails.length; i >= 0; i--) {
                Message message = emails[i -1];
                String subject = message.getSubject();
                if(subject.contains("BBCE Token")){
                    Object obj = message.getContent();
                    Multipart mp = (Multipart)obj;
                    mp.getBodyPart(0);
                    bodyMessage =  email.getTextFromBodyMessage(message);
                    return bodyMessage.substring(bodyMessage.indexOf(". Token") + 7).trim();
                }
            }
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
