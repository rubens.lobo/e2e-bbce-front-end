package br.com.pom.bbce.model;

public class DadosUsuario {

    private static String empresa;
    private static String email;
    private static String senhaEmail;
    private static String nome;
    private static String cpf;
    private static String senha;
    private static String senhaEletronica;
    private static String token;

    public static String getToken() {return token;}

    public static void setToken(String token) {DadosUsuario.token = token;}

    public static String getSenhaEmail() {return senhaEmail;}

    public static void setSenhaEmail(String senhaEmail) {DadosUsuario.senhaEmail = senhaEmail;}

    public static String getSenhaEletronica() {return senhaEletronica;}

    public static void setSenhaEletronica(String senhaEletronica) {DadosUsuario.senhaEletronica = senhaEletronica;}

    public static String getSenha() {return senha;}

    public static void setSenha(String senha) {DadosUsuario.senha = senha;}

    public static String getEmpresa() {
        return empresa;
    }

    public static void setEmpresa(String empresa) {
        DadosUsuario.empresa = empresa;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        DadosUsuario.email = email;
    }

    public static String getNome() {
        return nome;
    }

    public static void setNome(String nome) {
        DadosUsuario.nome = nome;
    }

    public static String getCpf() {
        return cpf;
    }

    public static void setCpf(String cpf) {
        DadosUsuario.cpf = cpf;
    }
}
