package br.com.pom.bbce.model;

public class DadosNegociacao {

    private static String produto;

    public static String getProduto() {
        return produto;
    }

    public static void setProduto(String produto) {
        DadosNegociacao.produto = produto;
    }
}
