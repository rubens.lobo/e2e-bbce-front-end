package br.com.pom.bbce.model;

public class DadosEmpresa {

    private static String cnpj;
    private static String razaoSocial;
    private static String nomeFantasia;
    private static String nire;
    private static String junta;
    private static String tipoEmpresa;
    private static String categoria;
    private static String siglaCCEE;
    private static String codCCEE;
    private static String tipoEnergia;
    private static String tipoAcordo;


    public static String getTipoAcordo() {
        return tipoAcordo;
    }

    public static void setTipoAcordo(String tipoAcordo) {
        DadosEmpresa.tipoAcordo = tipoAcordo;
    }

    public static String getCnpj() {
        return cnpj;
    }

    public static void setCnpj(String cnpj) {
        DadosEmpresa.cnpj = cnpj;
    }

    public static String getRazaoSocial() {
        return razaoSocial;
    }

    public static void setRazaoSocial(String razaoSocial) {
        DadosEmpresa.razaoSocial = razaoSocial;
    }

    public static String getNomeFantasia() {
        return nomeFantasia;
    }

    public static void setNomeFantasia(String nomeFantasia) {
        DadosEmpresa.nomeFantasia = nomeFantasia;
    }

    public static String getNire() {
        return nire;
    }

    public static void setNire(String nire) {
        DadosEmpresa.nire = nire;
    }

    public static String getJunta() {
        return junta;
    }

    public static void setJunta(String junta) {
        DadosEmpresa.junta = junta;
    }

    public static String getTipoEmpresa() {
        return tipoEmpresa;
    }

    public static void setTipoEmpresa(String tipoEmpresa) {
        DadosEmpresa.tipoEmpresa = tipoEmpresa;
    }

    public static String getCategoria() {
        return categoria;
    }

    public static void setCategoria(String categoria) {
        DadosEmpresa.categoria = categoria;
    }

    public static String getSiglaCCEE() {
        return siglaCCEE;
    }

    public static void setSiglaCCEE(String siglaCCEE) {
        DadosEmpresa.siglaCCEE = siglaCCEE;
    }

    public static String getCodCCEE() {
        return codCCEE;
    }

    public static void setCodCCEE(String codCCEE) {
        DadosEmpresa.codCCEE = codCCEE;
    }

    public static String getTipoEnergia() {
        return tipoEnergia;
    }

    public static void setTipoEnergia(String tipoEnergia) {
        DadosEmpresa.tipoEnergia = tipoEnergia;
    }
}
