package br.com.pom.bbce.pages.empresa;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import java.util.List;

public class MudarStatusEmpresa extends DriverManager implements Constantes {

    private By cbbAcao = By.name("acao_massa");
    private By btnAplicar = By.xpath("//div/a[text()='Aplicar']");
    private By statusAtivo = By.xpath("//div[@class='ui-grid-canvas']//div[text()='Ativo']");
    private By statusInativo = By.xpath("//div[@class='ui-grid-canvas']//div[text()='Em análise']");


    public void selecionarEmpresa() {
        List<WebElement> empresasRetornadas = getBrowser().findElements(By.xpath("//div[@class='ui-grid-canvas']//div[@class='ui-grid-cell-contents']"));

        if (empresasRetornadas.size() == 1) {
            empresasRetornadas.get(0).click();
        } else {
            if (empresasRetornadas.size() > 1) {
                Assert.fail("A pesquisa retornou mais de uma Empresa para seleção!");
            } else {
                Assert.fail("Valor inválido!");
            }
        }
        ExtentReports.appendToReport(getBrowser());
    }

    public String nomeRazaoSocial() {
        return getBrowser().findElement(By.xpath("//div/a[@class='ng-binding']")).getText();
    }

    public String cnpj() {
        return getBrowser().findElements(By.xpath("//div[@class='ui-grid-cell-contents ng-binding ng-scope']")).get(3).getText();
    }

    public void selecionarAcao(String option) {
        switch (option.toLowerCase()) {
            case "ativo":
                Select acaoAtivar = new Select(getBrowser().findElement(cbbAcao));
                acaoAtivar.selectByVisibleText("Ativar selecionados");
                break;
            case "inativo":
                Select acaoInativar = new Select(getBrowser().findElement(cbbAcao));
                acaoInativar.selectByVisibleText("Inativar selecionados");
                break;
            default:
                Assert.fail("Opção não localizada: " + option + ". Informe uma das opções ativo ou inativo!");
        }
        ExtentReports.appendToReport(getBrowser());
    }

    public void acionarBtnAplicar() {
        Action.clickOnElement(getBrowser(), btnAplicar, timeout);
        Verifications.wait(3);
        ExtentReports.appendToReport(getBrowser());
    }

    public boolean status(String statusEsperado) {
        switch (statusEsperado.toLowerCase()) {
            case "ativo":
                Verifications.verifyElementExists(getBrowser(), statusAtivo, timeout);
                return true;
            case "inativo":
                Verifications.verifyElementExists(getBrowser(), statusInativo, timeout);
                return true;
            default:
                Assert.fail("Opção não localizada: " + statusEsperado + ". Informe uma das opções ativo ou inativo!");
        }
        return false;
    }

    public void validarStatusAlterado(String statusEsperado) {
        try {
            Assert.assertTrue(status(statusEsperado), "O status da empresa não foi alterado com sucesso!");
        } catch (Error error) {
            Assert.fail(error.getMessage());
        }
        ExtentReports.appendToReport(getBrowser());

    }

}
