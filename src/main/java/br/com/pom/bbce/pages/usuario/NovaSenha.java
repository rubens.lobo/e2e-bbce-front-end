package br.com.pom.bbce.pages.usuario;

import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.core.view.ComboBoxActions;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.model.DadosUsuario;
import com.paulhammant.ngwebdriver.ByAngular;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class NovaSenha extends DriverManager implements Constantes {

    private By txtNovaSenha = By.name("newPass");
    private By txtConfirmaSenha = By.name("userPass2");
    private By btnGerarNovaSenha = By.xpath("//input[@ng-click='vm.login()']");

    public void confirmaNovaSenha(DataTable dataTable){
        List<Map<String,String>> table = dataTable.asMaps();

        for (Map<String,String> row : table){
            for(String campo : row.keySet()){
                String resposta = row.get(campo);
                switch (campo.toLowerCase()){
                    case "senha":
                        if(resposta.isEmpty()){
                            Action.setText(getBrowser(),txtNovaSenha,DadosUsuario.getSenha(),timeout);
                            Action.setText(getBrowser(),txtConfirmaSenha,DadosUsuario.getSenha(),timeout);
                            ExtentReports.appendToReport("Nova Senha para o Usuário: "+DadosUsuario.getSenha());
                        }else{
                            Action.setText(getBrowser(),txtNovaSenha,resposta,timeout);
                            Action.setText(getBrowser(),txtConfirmaSenha,resposta,timeout);
                            ExtentReports.appendToReport("Nova Senha para o Usuário: "+resposta);
                        }
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }
        ExtentReports.appendToReport(getBrowser());
        Action.clickOnElement(getBrowser(),btnGerarNovaSenha,timeout);
    }


}
