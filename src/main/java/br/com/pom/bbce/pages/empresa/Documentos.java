package br.com.pom.bbce.pages.empresa;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import org.openqa.selenium.By;

public class Documentos extends DriverManager implements Constantes {

    private By btnCadastrar = By.xpath("//button[@ng-click='vm.ok()'][text()='Cadastrar']");

    public void acionarBtnCadastrar() {
        Verifications.verifyElementIsClickable(getBrowser(), btnCadastrar, timeout);
        ExtentReports.appendToReport(getBrowser());
        Action.clickOnElement(getBrowser(), btnCadastrar, timeout);
    }



}
