package br.com.pom.bbce.pages.negociacao;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import com.paulhammant.ngwebdriver.ByAngular;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.Color;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Ordem extends DriverManager implements Constantes {

    private By txtVolumeVenda = ByAngular.model("vm.paperSelected.melhorOferta.quantidadeVenda");
    private By txtVolumeCompra = ByAngular.model("vm.paperSelected.melhorOferta.quantidadeCompra");
    private By txtValorCompra = ByAngular.model("vm.paperSelected.melhorOferta.precoVenda");
    private By txtValorVenda = ByAngular.model("vm.paperSelected.melhorOferta.precoCompra");
    private By ckbConfirmaValor = ByAngular.model("vm.savePriceAlert");
    private By ckbSalvarAssinatura = ByAngular.model("vm.saveSignature");
    private By txtAssinaturaEletronica = ByAngular.model("vm.paper.signature");
    private By btnConfirmar = By.xpath("//button[contains(text(),'Confirmar')]");
    private String tipoOrdem;
    private By ppAtencao = By.xpath("//div[@uib-modal-window='modal-window']//h2[text()='ATENÇÃO']");
    private By ppSucesso = By.xpath("//div[@uib-modal-window='modal-window']//*[text()='Sucesso']");
    private By btnConfirmo = By.xpath("//button[contains(text(),'Confirmo')]");
    private By rowTbProdutoSelecionado = By.xpath("//body/div[@class='main-view ng-scope']//div[contains(@ng-repeat, 'item in vm.papersShown')][contains(@class, 'highlight-item')]");

    public void solicitaOrdem(DataTable dataTable) {

        List<Map<String, String>> table = dataTable.asMaps();

        for (Map<String, String> row : table) {
            for (String campo : row.keySet()) {
                String resposta = row.get(campo);
                switch (campo.toLowerCase()) {
                    case "tipo":
                        this.tipoOrdem = resposta;
                        Action.clickOnElement(getBrowser(), By.xpath("//li[@heading='" + tipoOrdem + "']"), timeout);
                        break;
                    case "volume":
                        if (tipoOrdem.equalsIgnoreCase("Comprar")) {
                            Action.setText(getBrowser(), txtVolumeVenda, resposta, timeout);
                        } else {
                            Action.setText(getBrowser(), txtVolumeCompra, resposta, timeout);
                        }

                        break;
                    case "valor":
                        if (tipoOrdem.equalsIgnoreCase("Comprar")) {
                            Action.setText(getBrowser(), txtValorCompra, resposta, timeout);
                        } else {
                            Action.setText(getBrowser(), txtValorVenda, resposta, timeout);
                        }
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }
        Action.clickOnElement(getBrowser(), By.xpath("//span[contains(text(),'" + this.tipoOrdem + "')]"), timeout);
        Verifications.wait(1);
        ExtentReports.appendToReport(getBrowser());
    }

    public void Confirmar(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
/**
 * Adicionado o try catch para quando o "ckbConfirmaValor" não for exibido
 */
        try {
            Verifications.verifyElementExists(getBrowser(),ckbConfirmaValor,2);
            Action.clickOnElement(getBrowser(), ckbConfirmaValor, timeout);
        } catch (TimeoutException e) {
            System.out.println("Ignorar essa etapa!");
        }
        Action.clickOnElement(getBrowser(), ckbSalvarAssinatura, timeout);

        for (Map<String, String> row : table) {
            for (String campo : row.keySet()) {
                String resposta = row.get(campo);
                switch (campo.toLowerCase()) {
                    case "assinatura":
                        Action.setText(getBrowser(), txtAssinaturaEletronica, resposta, timeout);
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }
        ExtentReports.appendToReport(getBrowser());

        Action.clickOnElement(getBrowser(), btnConfirmar, timeout);
    }

    public boolean exibeModal(By modal) {
        Verifications.wait(1);
        if (getBrowser().findElements(modal).size() > 0) {
            return true;
        }
        return false;
    }

    public void confirmarNegociacao() {
        if (exibeModal(ppAtencao)) {
            ExtentReports.appendToReport(getBrowser());
            Verifications.verifyElementIsClickable(getBrowser(), btnConfirmo, timeout);
            Action.clickOnElement(getBrowser(), btnConfirmo, timeout);
        }
        ExtentReports.appendToReport(getBrowser());
        Verifications.verifyElementExists(getBrowser(), ppSucesso, timeout);
    }

    public void validarMatch() {
        String cor = getBrowser().findElement(rowTbProdutoSelecionado).getCssValue("background-color");
        String hex = Color.fromString(cor).asHex();
        System.out.println("Cor = " +hex);
        Assert.assertTrue(hex.contains("#b8dcf6"), "Cor "+hex+" diferente da cor esperada: #b8dcf6");
    }
}
