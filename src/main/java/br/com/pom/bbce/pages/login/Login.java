package br.com.pom.bbce.pages.login;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.core.view.ComboBoxActions;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.email.ReadEmail;
import br.com.pom.bbce.model.DadosUsuario;
import com.paulhammant.ngwebdriver.ByAngular;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Login extends DriverManager implements Constantes {

    private By txtEmail = ByAngular.model("vm.userLogin");
    private By txtSenha = By.id("userPass");
    private By cbbEmpresa = ByAngular.model("vm.empresaId");
    private  By btnEntrar = By.xpath("//input[@class='btn btn-primary']");

    public void acessaAplicacao(){
        getBrowser().get(urlPreProdBbce);
    }

    public void validaTelaLogin(){
        Verifications.verifyElementIsClickable(getBrowser(),txtEmail,timeout);
    }

    public void executaLogin(DataTable dataTable){

        List<Map<String,String>> table = dataTable.asMaps();

        for (Map<String,String> row : table){
            for(String campo : row.keySet()){
                String resposta = row.get(campo);
                switch (campo.toLowerCase()){
                    case "email":
                        if(resposta.isEmpty()){
                            Action.setText(getBrowser(),txtEmail,DadosUsuario.getEmail(),timeout);
                            getBrowser().findElement(txtSenha).click();
                        }else{
                            Action.setText(getBrowser(),txtEmail,resposta,timeout);
                            getBrowser().findElement(txtSenha).click();
                        }
                        break;
                    case "senha":
                        if(resposta.isEmpty()){
                            Action.setText(getBrowser(),txtSenha,DadosUsuario.getSenha(),timeout);
                        }else{
                            Action.setText(getBrowser(),txtSenha,resposta,timeout);
                        }
                        break;
                    case "empresa":
                        if(resposta.isEmpty()){
                            try{
                                ComboBoxActions.selectComboOptionByText(getBrowser(),cbbEmpresa,DadosUsuario.getEmpresa(),10);
                            }catch (Exception e){
                                System.out.println("Pulou o preenchimento combo-box empresa!");
                            }

                        }else{
                            try{
                                ComboBoxActions.selectComboOptionByText(getBrowser(),cbbEmpresa,resposta,10);
                            }catch (Exception e){
                                System.out.println("Pulou o preenchimento combo-box empresa!");
                            }
                        }
                        break;
                    case "limpa email":
                        if(resposta.equalsIgnoreCase("true")){
                            ReadEmail email = new ReadEmail(DadosUsuario.getEmail(),DadosUsuario.getSenhaEmail());
                            email.limpaCaixaDeEmails();
                        }
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }
        ExtentReports.appendToReport(getBrowser());
        getBrowser().findElement(btnEntrar).click();

    }
}
