package br.com.pom.bbce.pages.empresa;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import com.paulhammant.ngwebdriver.ByAngular;
import org.openqa.selenium.By;

public class CriarNovaEmpresa extends DriverManager implements Constantes {

    private By txtFiltro = ByAngular.model("colFilter.term");
    private By btnNovaEmpresa = By.xpath("//div/a[text()='Nova empresa']");

    public void validarBtn(String btn) {
        Verifications.verifyElementIsClickable(getBrowser(), By.xpath("//a[text()='" + btn + "']"), timeout);
    }

    public void acionarBtnNovaEmpresa() {
        Action.clickOnElement(getBrowser(), btnNovaEmpresa, timeout);
        ExtentReports.appendToReport(getBrowser());
    }

    public void preencherFiltroPesquisaEmp(String razaoSocial) {
        getBrowser().findElements(txtFiltro).get(0).clear();
        getBrowser().findElements(txtFiltro).get(0).sendKeys(razaoSocial);
        Verifications.wait(2);
    }

    public void preencherFiltroPesquisaCNPJ(String cnpj) {

        /**
         * Retirando o primeiro caracter do CNPJ quando este for igual a zero
         */
        char primeira = cnpj.charAt(0);
        String primeiraLetra = Character.toString(primeira);

        if (primeiraLetra.equals("0")) {
            cnpj = cnpj.substring(1);
        }

        getBrowser().findElements(txtFiltro).get(4).clear();
        getBrowser().findElements(txtFiltro).get(4).sendKeys(cnpj);
        Verifications.wait(2);
    }

    public void validarResultadoFiltro(String filtro) {
        Verifications.verifyElementExists(getBrowser(), By.xpath("//div/a[text()='" + filtro + "']"), timeout);
        ExtentReports.appendToReport(getBrowser());
    }

}
