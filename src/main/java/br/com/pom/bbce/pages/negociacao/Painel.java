package br.com.pom.bbce.pages.negociacao;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.model.DadosNegociacao;
import com.paulhammant.ngwebdriver.ByAngular;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public class Painel extends DriverManager implements Constantes {

    private By clTbProduto = By.xpath("//div[@ng-click='vm.orderByProduct()']");
    private By rowTbProduto = ByAngular.repeater("item in vm.papersShown");
    private By btnClosePoupAlert = By.xpath("//button[@ng-click='vm.close()']");

    private void escolheTipoDeApresentacaoDasOfertas(String tipo){
        Verifications.wait(2);
        Action.clickOnElement(getBrowser(), By.xpath("//a[@ng-click=\"vm.loadProducts('"+tipo.toLowerCase()+"')\"]"),timeout);
        Verifications.wait(1);
        Action.clickOnElement(getBrowser(), By.xpath("//a[@ng-click=\"vm.loadProducts('"+tipo.toLowerCase()+"')\"]"),timeout);
        Verifications.verifyTextsExistingElement(getBrowser(),clTbProduto,"Produto",timeout);
    }

    private void clicaNoProduto(String nomeProduto){

        List<WebElement> produtos = getBrowser().findElements(rowTbProduto);

        for (int i = 0; i < produtos.size() ; i++) {
            if(produtos.get(i).getText()
                    .replace("\n","")
                    .replace("-","")
                    .trim().replace(" ","")
                    .contains(nomeProduto.replace(" ",""))){
                produtos.get(i).click();
                break;
            }
        }
    }

    public void selecionaProduto(DataTable dataTable){

        try{
            Action.clickOnElement(getBrowser(),btnClosePoupAlert,3);
        }catch (TimeoutException e){}

        List<Map<String,String>> table = dataTable.asMaps();
        for (Map<String,String> row : table){
            for(String campo : row.keySet()){
                String resposta = row.get(campo);
                switch (campo.toLowerCase()){
                    case "tipo":
                            escolheTipoDeApresentacaoDasOfertas(resposta);
                        break;
                    case "produto":
                        if(resposta.isEmpty()){
                            Assert.fail("Você deve informar um produto no step do gherkin!");
                        }else{
                            DadosNegociacao.setProduto(resposta);
                        }
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }
        clicaNoProduto(DadosNegociacao.getProduto());
        ExtentReports.appendToReport(getBrowser());
    }
}
