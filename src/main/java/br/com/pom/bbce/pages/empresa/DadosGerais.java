package br.com.pom.bbce.pages.empresa;

import br.com.core.asserts.Verifications;
import br.com.core.documents.Documents;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.model.DadosEmpresa;
import com.github.javafaker.Faker;
import com.paulhammant.ngwebdriver.ByAngular;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import java.util.Locale;

public class DadosGerais extends DriverManager implements Constantes {

    private By txtCnpj = ByAngular.model("vm.company.cnpj");
    private By txtRazaoSocial = ByAngular.model("vm.company.razaoSocial");
    private By txtNomeFantasia = ByAngular.model("vm.company.nomeFantasia");
    private By cbbTipoEmpresa =  By.id("submarket");
    private By cbbCategoria = By.id("categoria");
    private By txtNire = ByAngular.model("vm.company.nire");
    private By txtJunta = ByAngular.model("vm.company.juntaComercial");
    private By checkProdNegociado = By.xpath("//tr/td[text()='Energia Elétrica']/following-sibling::td//input[contains(@ng-model,'product.negociaFisico')]");
    private By btnCadastrar = By.xpath("//form[@ng-submit='vm.ok()']//button[contains(text(),'Cadastrar')]");

    public void preencherCampos(String tipoEmpresa, String categoria) {

        Faker dadosFaker = new Faker(new Locale("pt-BR"));
        DadosEmpresa.setCnpj(Documents.getCnpj(false));
        DadosEmpresa.setRazaoSocial(dadosFaker.beer().name());
        DadosEmpresa.setNomeFantasia(DadosEmpresa.getRazaoSocial());
        DadosEmpresa.setTipoEmpresa(tipoEmpresa);
        DadosEmpresa.setCategoria(categoria);
        DadosEmpresa.setNire(dadosFaker.number().digits(11));
        DadosEmpresa.setJunta(dadosFaker.number().digits(12));

        Verifications.verifyElementIsVisible(getBrowser(), txtCnpj, timeout);
        Action.setText(getBrowser(), txtCnpj, DadosEmpresa.getCnpj(), timeout);
        Action.setText(getBrowser(), txtRazaoSocial, DadosEmpresa.getRazaoSocial(), timeout);
        Action.setText(getBrowser(), txtNomeFantasia, DadosEmpresa.getNomeFantasia(), timeout);
        Select tipoEmpSelected = new Select(getBrowser().findElement(cbbTipoEmpresa));
        tipoEmpSelected.selectByVisibleText(tipoEmpresa);
        Select categoriaSelected = new Select(getBrowser().findElement(cbbCategoria));
        categoriaSelected.selectByVisibleText(categoria);
        Action.setText(getBrowser(), txtNire, DadosEmpresa.getNire(), timeout);
        Action.setText(getBrowser(), txtJunta, DadosEmpresa.getJunta(), timeout);
        Action.clickOnElement(getBrowser(), checkProdNegociado, timeout);
        ExtentReports.appendToReport(getBrowser(),"Empresa: " +DadosEmpresa.getRazaoSocial());
    }

    public void acionarBtnCadastrar() {
        Verifications.verifyElementIsClickable(getBrowser(), btnCadastrar, timeout);
        Action.clickOnElement(getBrowser(), btnCadastrar, timeout);
    }
}
