package br.com.pom.bbce.pages.usuario;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.model.DadosUsuario;
import com.paulhammant.ngwebdriver.ByAngular;
import org.openqa.selenium.By;

public class TermoDeUso extends DriverManager implements Constantes {

    private By txtAsinaturaEletronica = ByAngular.model("vm.assinaturaEletronica");
    private By btnAssinarAvancar = By.xpath("//button[@ng-click='vm.assinaDocumentos()']");

    public void assinarTermoDeUso(){
        Verifications.verifyElementIsClickable(getBrowser(),txtAsinaturaEletronica,timeout);
        ExtentReports.appendToReport(getBrowser());
        Action.setText(getBrowser(),txtAsinaturaEletronica, DadosUsuario.getSenhaEletronica(),timeout);
        Action.clickOnElement(getBrowser(),btnAssinarAvancar,timeout);
    }
}
