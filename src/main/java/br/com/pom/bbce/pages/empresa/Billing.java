package br.com.pom.bbce.pages.empresa;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class Billing extends DriverManager implements Constantes {
    
    private By cbbTipoAcordo = By.id("comboTipoAcordo");
    private By btnFinalizar = By.xpath("//button[text()='Finalizar']");

        public void preencherCampos(String tipoAcordo) {
        Select tipoAcordoSelected = new Select(getBrowser().findElement(cbbTipoAcordo));
        tipoAcordoSelected.selectByVisibleText(tipoAcordo);
        ExtentReports.appendToReport(getBrowser());
    }

    public void acionarBtnFinalizar() {
        Verifications.verifyElementIsClickable(getBrowser(), btnFinalizar, timeout);
        Action.clickOnElement(getBrowser(), btnFinalizar, timeout);
    }

}
