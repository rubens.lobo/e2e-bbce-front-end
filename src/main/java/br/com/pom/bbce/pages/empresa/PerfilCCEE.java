package br.com.pom.bbce.pages.empresa;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.model.DadosEmpresa;
import com.github.javafaker.Faker;
import com.paulhammant.ngwebdriver.ByAngular;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import java.util.Locale;

public class PerfilCCEE extends DriverManager implements Constantes {

    private By btnAddPerfil = By.xpath("//a[@ng-click='vm.addProfileRow()']");
    private By txtSiglaCCEE = By.xpath("//label[text()='Sigla CCEE']/following-sibling::input");
    private By txtCodCCEE = By.xpath("//label[text()='Código CCEE']/following-sibling::input");
    private By cbbTipoEnergia = ByAngular.model("profile.tipoEnergia");
    private By btnCadastrar = By.xpath("//form[@ng-submit='vm.saveProfiles()']//button[contains(text(),'Cadastrar')]");

    public void preencherCampos(String tipoEnergia) {

        Faker dadosFaker = new Faker(new Locale("pt-BR"));
        DadosEmpresa.setSiglaCCEE(dadosFaker.stock().nsdqSymbol());
        DadosEmpresa.setCodCCEE(dadosFaker.number().digits(12));

        Verifications.verifyElementIsClickable(getBrowser(), btnAddPerfil, timeout);
        Action.clickOnElement(getBrowser(), btnAddPerfil, timeout);
        Action.setText(getBrowser(), txtSiglaCCEE, DadosEmpresa.getSiglaCCEE(), timeout);
        Action.setText(getBrowser(), txtCodCCEE, DadosEmpresa.getCodCCEE(), timeout);
        Select tipoEnergiaSelected = new Select(getBrowser().findElement(cbbTipoEnergia));
        tipoEnergiaSelected.selectByVisibleText(tipoEnergia);
        ExtentReports.appendToReport(getBrowser());
    }

    public void acionarBtnCadastrar() {
        Verifications.verifyElementIsClickable(getBrowser(), btnCadastrar, timeout);
        Action.clickOnElement(getBrowser(), btnCadastrar, timeout);
    }

}
