package br.com.pom.bbce.pages.usuario;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.pom.bbce.constantes.Constantes;
import org.openqa.selenium.By;

public class Assinados extends DriverManager implements Constantes {

    private By btnAcessarBalcao = By.xpath("//button");

    public void validaTermoDeUsoAplicado(String msg){
        Verifications.verifyElementExists(getBrowser(), By.xpath("//*[contains(text(),'"+msg+"')]"),timeout);
        ExtentReports.appendToReport(getBrowser());
        getBrowser().findElement(btnAcessarBalcao).click();
    }
}
