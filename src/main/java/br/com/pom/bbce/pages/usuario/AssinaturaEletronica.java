package br.com.pom.bbce.pages.usuario;

import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.model.DadosUsuario;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class AssinaturaEletronica  extends DriverManager implements Constantes {

    private By txtAssinaturaEletronica = By.id("electronic-signature");
    private By txtConfirmaAssinatura = By.id("corfirm-signature");
    private By btnSalvarAssinatura = By.xpath("//button[@ng-click='vm.requestNewSignature()']");

    public void confirmaAssinaturaEletronica(DataTable dataTable){

        List<Map<String,String>> table = dataTable.asMaps();

        for (Map<String,String> row : table){
            for(String campo : row.keySet()){
                String resposta = row.get(campo);
                switch (campo.toLowerCase()){
                    case "assinatura":
                        Action.setText(getBrowser(),txtAssinaturaEletronica,resposta,timeout);
                        Action.setText(getBrowser(),txtConfirmaAssinatura,resposta,timeout);
                        DadosUsuario.setSenhaEletronica(resposta);
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }
        ExtentReports.appendToReport(getBrowser());
        Action.clickOnElement(getBrowser(),btnSalvarAssinatura,timeout);
    }


}
