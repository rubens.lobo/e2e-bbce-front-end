package br.com.pom.bbce.pages.menuprincipal;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.model.DadosUsuario;
import org.openqa.selenium.By;

public class MenuPrincipal extends DriverManager implements Constantes {

    private By btnNovoUsuarioSistema = By.xpath("//a[@ng-click='vm.newUser()']");
    private By btnLogout = By.xpath("//div[@id='navbar-painel-content']//div[contains(@class,'main-menu')]//a[@uib-tooltip='Sair']");

    public void validaTelaMenuPrincipal () {
        Verifications.verifyElementIsClickable(getBrowser(),btnLogout, timeout);
        ExtentReports.appendToReport(getBrowser());
    }

    public void clicaNoMenu(String item){
            getBrowser().findElement(By.xpath("//a[contains(text(),'"+item+"')]")).click();
    }

    /**
     *  Botao "Novo DadosUsuario Sistema" - verify + click
     */
    public void clicaBotaoNovoUsuario() {
        Verifications.wait(2);
        Action.clickOnElement(getBrowser(),btnNovoUsuarioSistema,timeout);
    }

    public void validaUsuarioCriadoComSucesso(String msg){
        Verifications.verifyElementExists(getBrowser(),By.xpath("//div[contains(text(),'"+msg+"')]"),timeout);
        ExtentReports.appendToReport(getBrowser());
    }

    public void validaTelaMenuUsuario(){
        Verifications.verifyElementExists(getBrowser(),btnLogout,timeout);
        ExtentReports.appendToReport(getBrowser());
    }

    public void executaLogout(){
        Verifications.wait(2);
        Action.clickOnElement(getBrowser(), btnLogout,timeout);
    }

}
