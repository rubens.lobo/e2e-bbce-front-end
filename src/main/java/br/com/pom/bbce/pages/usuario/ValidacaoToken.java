package br.com.pom.bbce.pages.usuario;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.email.ReadEmail;
import br.com.pom.bbce.model.DadosUsuario;
import com.paulhammant.ngwebdriver.ByAngular;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ValidacaoToken extends DriverManager implements Constantes {

    private By txtToken = ByAngular.model("modal.token");
    private By lblTokenEnviado = By.xpath("//div[contains(text(),'Token gerado com sucesso. Verifique na caixa de e-mail')]");
    private By btnVerificarToken = By.xpath("//button[@ng-click='modal.checkToken()']");

    public void validaEmailEnviado(){
        Verifications.verifyElementExists(getBrowser(),lblTokenEnviado,timeout);
    }

    public void recuperaToken(){
        ReadEmail readEmail = new ReadEmail(DadosUsuario.getEmail(),DadosUsuario.getSenhaEmail());
        DadosUsuario.setToken(readEmail.recuperaEmailComToken());
        ExtentReports.appendToReport("Token: " +DadosUsuario.getToken());
        Assert.assertNotNull(DadosUsuario.getToken(),"Erro ao recuperar Token no email - Tela Validação de Token");
    }

    public void enviaToken(){
        Action.setText(getBrowser(),txtToken,DadosUsuario.getToken(),timeout);
        ExtentReports.appendToReport(getBrowser());
        getBrowser().findElement(btnVerificarToken).click();
    }

}
