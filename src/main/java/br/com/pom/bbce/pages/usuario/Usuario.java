package br.com.pom.bbce.pages.usuario;

import br.com.core.asserts.Verifications;
import br.com.core.documents.Documents;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.core.view.ComboBoxActions;
import br.com.pom.bbce.constantes.Constantes;
import br.com.pom.bbce.model.DadosEmpresa;
import br.com.pom.bbce.model.DadosUsuario;
import com.github.javafaker.Faker;
import com.paulhammant.ngwebdriver.ByAngular;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;

public class Usuario extends DriverManager implements Constantes {

    private By btnUsuarioEmpresa = By.xpath("//a[@ng-click='vm.selectUserType(1);']");
    private By txtEmpresa = ByAngular.model("vm.user.company");
    private By cbbCodEmpresa = ByAngular.repeater("match in matches");
    private By txtEmail = ByAngular.model("vm.user.email");
    private By txtNomeCompleto = ByAngular.model("vm.user.nome");
    private By ckbSim = ByAngular.model("vm.user.representanteLegal");
    private By ckbAssinar = ByAngular.model("vm.user.assina");
    private By cbbCargo = ByAngular.model("vm.user.cargo");
    private By txtCpf = ByAngular.model("vm.user.cpf");
    private By abaControleAcesso = By.xpath("//a[contains(text(),'Controle')]");
    private By ckbBalcao = ByAngular.model("profile.value");
    private By formCadastroUsuario = By.name("vm.formUser");


    public void clicaBotaoUsuarioEmpresa() {
        Verifications.wait(1);
        Action.clickOnElement(getBrowser(),btnUsuarioEmpresa,timeout);
    }

    public void preencheDadosGerais(DataTable dataTable){
        final int ANALISTA_DE_FATURAMENTO = 16;
        Faker faker = new Faker(new Locale("pt-BR"));
        List<Map<String,String>> table = dataTable.asMaps();

        for (Map<String,String> row : table){
            for(String campo : row.keySet()){
                String resposta = row.get(campo);
                switch (campo.toLowerCase()){
                    case "empresa":
                        if(resposta.isEmpty()){
                            DadosUsuario.setEmpresa(DadosEmpresa.getRazaoSocial());
                        }else{
                            DadosUsuario.setEmpresa(resposta);
                        }



                        break;
                    case "email":
                        DadosUsuario.setEmail(resposta);
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }

        DadosUsuario.setCpf(Documents.getCpf(false));
        DadosUsuario.setNome(faker.name().fullName());

        Verifications.verifyElementIsClickable(getBrowser(),txtEmpresa,timeout);
        getBrowser().findElement(txtEmpresa).sendKeys(DadosUsuario.getEmpresa());
        Action.clickOnElement(getBrowser(),cbbCodEmpresa,timeout);
        Verifications.wait(2);
        getBrowser().findElement(txtEmail).sendKeys(DadosUsuario.getEmail());
        getBrowser().findElement(txtNomeCompleto).sendKeys(DadosUsuario.getNome());
        getBrowser().findElement(ckbSim).click();
        getBrowser().findElement(ckbAssinar).click();
        ComboBoxActions.selectComboOptionByIndex(getBrowser(),cbbCargo,ANALISTA_DE_FATURAMENTO,10);
        getBrowser().findElement(txtCpf).sendKeys(DadosUsuario.getCpf());
    }

    public void preencheControleAcesso(){
        Action.clickOnElement(getBrowser(),abaControleAcesso,timeout);
        getBrowser().findElement(ckbBalcao).click();
        Verifications.wait(1);
        getBrowser().findElement(formCadastroUsuario).submit();
        Verifications.wait(1);
        try {
            getBrowser().findElement(formCadastroUsuario).submit();
        }catch (NoSuchElementException e){

        }
    }




}
