package br.com.core.email;


import br.com.core.report.ExtentReports;
import com.mysql.cj.xdevapi.Type;
import org.jsoup.Jsoup;

import javax.mail.*;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Properties;

public class Email {

    private String host;
    private String port;
    private String user;
    private String pass;
    private Store store;
    private static final String SERVIDOR_RECEBIMENTO_HOST = "mail.imap.host";
    private static final String SERVIDOR_RECEBIMENTO_PORT = "mail.imap.port";

    /**
     *
     * @param host from your mail box
     * @param port from your mail box
     * @param user to be authenticated
     * @param pass to be authenticated
     */
    public Email(String host, String port, String user, String pass) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.pass = pass;
    }

    /**
     *  It connects this client to mail
     *
     * @return object with connection
     */
    public Store connect(String protocol){
        try {
            Properties properties = new Properties();
            properties.put(SERVIDOR_RECEBIMENTO_HOST, host);
            properties.put(SERVIDOR_RECEBIMENTO_PORT, port);
            properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.setProperty("mail.imap.socketFactory.fallback", "false");
            properties.setProperty("mail.imap.socketFactory.port", String.valueOf(getPort()));
            Session session = Session.getDefaultInstance(properties);
            store = session.getStore(protocol);
            store.connect(getUser(), getPass());
        } catch (MessagingException e ) {
            e.printStackTrace();
        }
        return store;
    }

    /**
     *  Close connection with mail server
     *
     */
    public void close(){
        try {
            store.close();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     *  getFolder from a mail
     *
     * @param store set object with connection from your mail
     * @param itemFromBoxMail set the name from a folder you want to get mails, example: Caixa de Entrada => INBOX
     * @param write set true to grant writing and reading mails or false to just read mails
     * @return folder with mails from the account
     */
    public Folder getFolder(Store store, String itemFromBoxMail, boolean write){
        Folder folder = null;
        try {
            folder = store.getFolder(itemFromBoxMail);
            if(write){
                folder.open(Folder.READ_WRITE);
            }else {
                folder.open(Folder.READ_ONLY);
            }

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return  folder;
    }


    /**
     *
     *  Get body message from the body mail
     *
     * @param message set object with emails
     * @return message body from the mail
     */
    public String getTextFromBodyMessage(Message message) {
        try {
            if (message.isMimeType("text/plain")){
                return message.getContent().toString();
            }else if (message.isMimeType("multipart/*")) {
                String result = "";
                MimeMultipart mimeMultipart = (MimeMultipart)message.getContent();
                int count = mimeMultipart.getCount();
                for (int i = 0; i < count; i ++){
                    BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                    if (bodyPart.isMimeType("text/plain")){
                        result = result + "\n" + bodyPart.getContent();
                        break;
                    } else if (bodyPart.isMimeType("text/html")){
                        String html = (String) bodyPart.getContent();
                        result = result + "\n" + Jsoup.parse(html).text();
                    }
                }
                return result;
            }
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     *  Delete all email from the box
     *
     * @param emails list of mails
     */
    public void deleteMessage(Message[] emails) {
        try {
            for (int i = 0; i < emails.length ; i++) {
                emails[i].setFlag(Flags.Flag.DELETED, true);
            }
        } catch (MessagingException e) {
            ExtentReports.appendToReport("Erro ao tentar deletar os email's");
        }
        ExtentReports.appendToReport("Todos email's deletados da caixa de entrada!");
    }



    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
