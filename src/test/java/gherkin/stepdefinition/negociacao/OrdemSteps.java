package gherkin.stepdefinition.negociacao;

import br.com.pom.bbce.pages.negociacao.Ordem;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import io.cucumber.datatable.DataTable;

public class OrdemSteps {

    private Ordem ordem = new Ordem();


    @E("aplicar valor de oferta")
    public void aplicarValorDeOferta(DataTable dataTable) {
        ordem.solicitaOrdem(dataTable);
    }

    @E("confirmo com assinatura eletronica")
    public void confirmoComAssinaturaEletronica(DataTable dataTable) {

        ordem.Confirmar(dataTable);
    }

    @Entao("sera apresentado a confirmacao da negociacao")
    public void seraApresentadoAConfirmacaoDaNegociacao() {
        ordem.confirmarNegociacao();
    }

    @Entao("sera apresentado a confirmacao da negociacao de Venda")
    public void seraApresentadoAConfirmacaoDaNegociacaoDeVenda() {
        ordem.confirmarNegociacao();
        ordem.validarMatch();

    }
}
