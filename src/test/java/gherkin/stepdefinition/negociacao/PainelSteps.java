package gherkin.stepdefinition.negociacao;

import br.com.pom.bbce.pages.negociacao.Painel;
import cucumber.api.java.pt.Quando;
import io.cucumber.datatable.DataTable;

public class PainelSteps {

    private Painel painel = new Painel();

    @Quando("eu escolher um produto")
    public void euEscolherUmProduto(DataTable dataTable) {
           painel.selecionaProduto(dataTable);
    }
}
