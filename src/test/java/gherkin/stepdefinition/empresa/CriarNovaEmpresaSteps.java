package gherkin.stepdefinition.empresa;

import br.com.pom.bbce.model.DadosEmpresa;
import br.com.pom.bbce.pages.empresa.*;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import io.cucumber.datatable.DataTable;
import org.jetbrains.annotations.NotNull;
import java.util.List;

public class CriarNovaEmpresaSteps {

    private CriarNovaEmpresa empresa = new CriarNovaEmpresa();
    private DadosGerais abaDadosGerais = new DadosGerais();
    private PerfilCCEE abaPerfilCCEE = new PerfilCCEE();
    private Documentos abaDocumentos = new Documentos();
    private Billing abaBilling = new Billing();
    private static String tipoEmpresa;
    private static String tipoCategoria;
    private static String tipoEnergia;
    private static String tipoAcordo;

    @Então("exibe o botao {string}")
    public void exibeOBotao(String btnName) {
        empresa.validarBtn(btnName);
    }

    @Dado("que a Nova empresa foi cadastrada com sucesso")
    public void queANovaEmpresaFoiCadastradaComSucesso(@NotNull DataTable dataTable) {

        List<List<String>> list = dataTable.asLists(String.class);

        for (int i = 1; i < list.size(); i++) {
            tipoEmpresa = list.get(i).get(0);
            tipoCategoria = list.get(i).get(1);
            tipoEnergia = list.get(i).get(2);
            tipoAcordo = list.get(i).get(3);

            empresa.acionarBtnNovaEmpresa();
            abaDadosGerais.preencherCampos(tipoEmpresa, tipoCategoria);
            abaDadosGerais.acionarBtnCadastrar();
            abaPerfilCCEE.preencherCampos(tipoEnergia);
            abaPerfilCCEE.acionarBtnCadastrar();
            abaDocumentos.acionarBtnCadastrar();
            abaBilling.preencherCampos(tipoAcordo);
            abaBilling.acionarBtnFinalizar();
            empresa.preencherFiltroPesquisaEmp(DadosEmpresa.getRazaoSocial());
            empresa.preencherFiltroPesquisaCNPJ(DadosEmpresa.getCnpj());
            empresa.validarResultadoFiltro(DadosEmpresa.getRazaoSocial());
        }
    }
}
