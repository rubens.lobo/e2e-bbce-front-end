package gherkin.stepdefinition.empresa;

import br.com.pom.bbce.pages.empresa.CriarNovaEmpresa;
import br.com.pom.bbce.pages.empresa.MudarStatusEmpresa;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class MudarStatusEmpresaSteps {

    private MudarStatusEmpresa empresaCadastrada = new MudarStatusEmpresa();
    private CriarNovaEmpresa localizarEmpresa = new CriarNovaEmpresa();
    public static String razaoSocial;
    public static String cnpj;

    @Quando("altera o status da empresa para {string}")
    public void alteraOStatusDaEmpresa(String status) {
        empresaCadastrada.selecionarAcao(status);
        empresaCadastrada.selecionarEmpresa();
        razaoSocial = empresaCadastrada.nomeRazaoSocial();
        cnpj = empresaCadastrada.cnpj().replace(".", "").replace("/", "").replace("-","");
        empresaCadastrada.acionarBtnAplicar();
    }

    @Então("o sistema exibe a empresa com status {string}")
    public void oSistemaExibeAEmpresaComStatusAtivo(String status) {
        localizarEmpresa.preencherFiltroPesquisaEmp(razaoSocial);
        localizarEmpresa.preencherFiltroPesquisaCNPJ(cnpj);
        localizarEmpresa.validarResultadoFiltro(razaoSocial);
        empresaCadastrada.validarStatusAlterado(status);
    }
}
