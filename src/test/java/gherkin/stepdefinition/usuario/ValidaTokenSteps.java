package gherkin.stepdefinition.usuario;

import br.com.pom.bbce.pages.usuario.ValidacaoToken;
import cucumber.api.java.pt.E;

public class ValidaTokenSteps {

    private ValidacaoToken validacaoToken = new ValidacaoToken();

    @E("validar token gerado")
    public void validarTokenGerado() {
        validacaoToken.validaEmailEnviado();
        validacaoToken.recuperaToken();
        validacaoToken.enviaToken();
    }
}
