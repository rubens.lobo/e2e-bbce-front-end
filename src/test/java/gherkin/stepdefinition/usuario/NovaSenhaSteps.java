package gherkin.stepdefinition.usuario;

import br.com.pom.bbce.pages.usuario.NovaSenha;
import cucumber.api.java.pt.Quando;
import io.cucumber.datatable.DataTable;

public class NovaSenhaSteps {

    NovaSenha novaSenha = new NovaSenha();

    @Quando("eu confirmar uma nova senha para o usuario")
    public void euConfirmarUmaNovaSenhaParaOUsuario(DataTable dataTable) {
        novaSenha.confirmaNovaSenha(dataTable);
    }


}
