package gherkin.stepdefinition.usuario;

import br.com.pom.bbce.pages.usuario.Usuario;
import cucumber.api.java.pt.E;
import io.cucumber.datatable.DataTable;

public class UsuarioSteps {

    private Usuario usuario = new Usuario();

    @E("eu cadastro um usuario no sistema")
    public void euCadastroUmUsuarioNoSistema(DataTable table) {
        usuario.clicaBotaoUsuarioEmpresa();
        usuario.preencheDadosGerais(table);
        usuario.preencheControleAcesso();
    }

}
