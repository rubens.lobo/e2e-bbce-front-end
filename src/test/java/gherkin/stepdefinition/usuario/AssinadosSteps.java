package gherkin.stepdefinition.usuario;

import br.com.pom.bbce.pages.usuario.Assinados;
import br.com.pom.bbce.pages.menuprincipal.MenuPrincipal;
import br.com.pom.bbce.pages.usuario.TermoDeUso;
import cucumber.api.java.pt.Entao;

public class AssinadosSteps {

    private TermoDeUso termoDeUso = new TermoDeUso();
    private Assinados assinados = new Assinados();
    private MenuPrincipal menuPrincipal = new MenuPrincipal();

    @Entao("sera apresentada a mensagem {string} na tela assinados")
    public void seraApresentadaAMensagemNaTelaAssinados(String msg) {
        termoDeUso.assinarTermoDeUso();
        assinados.validaTermoDeUsoAplicado(msg);
        menuPrincipal.validaTelaMenuUsuario();
    }
}
