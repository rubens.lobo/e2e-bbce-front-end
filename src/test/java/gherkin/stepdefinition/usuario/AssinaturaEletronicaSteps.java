package gherkin.stepdefinition.usuario;

import br.com.pom.bbce.pages.usuario.AssinaturaEletronica;
import br.com.pom.bbce.pages.usuario.ValidacaoToken;
import cucumber.api.java.pt.E;
import io.cucumber.datatable.DataTable;

public class AssinaturaEletronicaSteps {

    private AssinaturaEletronica assinaturaEletronica = new AssinaturaEletronica();
    private ValidacaoToken validacaoToken = new ValidacaoToken();

    @E("criar a assinatura eletronica")
    public void criarAssinaturaEletronica(DataTable dataTable) {
        assinaturaEletronica.confirmaAssinaturaEletronica(dataTable);
    }

}
