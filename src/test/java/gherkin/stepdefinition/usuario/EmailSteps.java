package gherkin.stepdefinition.usuario;


import br.com.core.report.ExtentReports;
import br.com.pom.bbce.email.ReadEmail;
import br.com.pom.bbce.model.DadosUsuario;
import br.com.pom.bbce.pages.usuario.ValidacaoToken;
import cucumber.api.java.pt.Entao;
import org.testng.Assert;

public class EmailSteps {

    ValidacaoToken validacaoToken = new ValidacaoToken();

    @Entao("o usuario recebera um email com uma senha provisoria")
    public void o_usuario_recebera_um_email_com_uma_senha_provisoria() {
        ReadEmail readEmail = new ReadEmail(DadosUsuario.getEmail(),DadosUsuario.getSenhaEmail());
        DadosUsuario.setSenha(readEmail.recuperaEmailComSenhaInicial());
        ExtentReports.appendToReport("Senha inicial: " +DadosUsuario.getSenha());
        Assert.assertNotNull(DadosUsuario.getSenha());
    }
}
