package gherkin.stepdefinition.menuprincipal;
import br.com.pom.bbce.pages.menuprincipal.MenuPrincipal;
import cucumber.api.java.pt.*;

public class MenuPrincipalSteps {

    private MenuPrincipal menuPrincipal = new MenuPrincipal();

    @Entao("sera apresentada tela do menu principal")
    public void seraApresentadaTelaDoMenuPrincipal() {
        menuPrincipal.validaTelaMenuPrincipal();
    }

    @Quando("eu clico no item do menu {string}")
    public void euClicoNoItemDoMenu(String item) {
        menuPrincipal.clicaNoMenu(item);
    }

    @E("eu clico no botao novo Usuario Sistema")
    public void euClicoNoBotaoNovoUsuarioSistema() {
        menuPrincipal.clicaBotaoNovoUsuario();
    }

    @Entao("sera apresentada a mensagem {string}")
    public void sera_apresentada_a_mensagem(String msg) {
        menuPrincipal.validaUsuarioCriadoComSucesso(msg);
    }

    @Dado("eu faco logoff na aplicacao")
    public void euFacoLogoffNaAplicacao() {
        menuPrincipal.executaLogout();
    }
}
