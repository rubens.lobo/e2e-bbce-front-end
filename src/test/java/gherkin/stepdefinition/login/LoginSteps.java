package gherkin.stepdefinition.login;

import br.com.pom.bbce.pages.login.Login;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Quando;
import io.cucumber.datatable.DataTable;

public class LoginSteps {

    private Login login = new Login();

    @Dado("Eu estou na pagina de login")
    public void eu_estou_na_pagina_de_login() {
        login.acessaAplicacao();
    }

    @Quando("eu logar na aplicacao com as credenciais")
    public void euLogarComUsuarioESenha(DataTable dataTable) {
        login.validaTelaLogin();
        login.executaLogin(dataTable);
    }

}
