package gherkin.hook;


import br.com.core.asserts.Verifications;
import br.com.core.integration.OpenStf;
import br.com.core.properties.PropertiesManager;
import br.com.core.report.ExtentReports;
import br.com.core.setup.AppWeb;
import br.com.core.setup.DriverManager;
import br.com.core.setup.Drivers;
import br.com.core.view.Action;
import br.com.gft.Encrypt;
import br.com.pom.bbce.email.ReadEmail;
import br.com.pom.bbce.model.DadosUsuario;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import sun.misc.BASE64Decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static br.com.core.setup.Drivers.closeDriver;
import static io.restassured.RestAssured.given;
import static io.restassured.path.xml.XmlPath.with;


public class Hook extends ExtentReports {

    @Before
    public void init(Scenario scenario) {
        testScenario.set(scenario);
        PropertiesManager propertiesManager = new PropertiesManager("src\\test\\resources\\bbce.properties");
        DadosUsuario.setEmail(propertiesManager.getProps().getProperty("user.email"));
        DadosUsuario.setSenhaEmail(propertiesManager.getProps().getProperty("user.senha"));

        AppWeb app = new AppWeb();
        app.setTestName(DriverManager.testScenario.get().getName() +" - "+ System.getProperty("browser"));
        app.setBrowserName("chrome");
        app.setUpDriver(app);
    }



    @After
    public void cleanUp() {
        Drivers.closeDriver(getBrowser());

    }
}









