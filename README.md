# E2E-BBCE-FRONT-END

Projeto voltado ao desenvolvimento de testes automatizados para a aplicação BBCE

*   Empresa
    * [CT001 - BBCE - Criar Empresa](feature/01-Empresa.feature)
*   Usuario
    * [CT001 - BBCE - Criar Usuário](feature/02-Usuario.feature)

~~~~
Para criar um usuário, será necessário cria um email gmail, único homologado para automação com estes scripts.

1º Criar email
2º Habilitar os protocolos de comunicação POP e IMAP, acesse seu email e depois insira na url este link
   "https://mail.google.com/mail/u/0/#settings/fwdandpop" ele irá te direcionar para a página certa de
   configuração, depois de configurar bastar salvar as alterações realizadas.
~~~~

<div align="center">
    <img id="header" src="./src/test/resources/img/settings-mail.png" />
</div>

~~~~
3º No email, acesse o icone ao lado direito da tela "opções" e acesse a sua conta do google e depois a opção
   "Segurança" e habilite o acesso a aplicativos menos seguro.
~~~~

<div align="center">
    <img id="header" src="./src/test/resources/img/less-secure-app-acess.png" />
</div>

~~~~
4º Insira o usuário e senha do email no arquivo de properties

    Localizado no diretório: src\test\resources\bbce.properties
~~~~

*   Negociacao
    * [CT001 - BBCE - Negociação de energia compra e venda](feature/03-Negociacao.feature)


## PRÉ-REQUISITOS 

Descreva os requisitos de software e hardware que é necessário para executar este projeto de automação

*   Java 1.8 SDK
*   Maven 3.5.*
*   Intellij
    * Plugin Cucumber for java

## EXECUTANDO OS TESTES

```
Nós criamos dois modelos, onde você poderá executar os teste de forma integrada ou não, isso significa que um teste
vai depender do outro, forma lógica:

1º Criar empresa
2º Criar Usuário
3º Efetuar as negociações

Se optar por utilizar de forma individual, basta preencher a massa de dados dentro do arquivo de features.
```

## EMPRESA DESENVOLVEDORA

GFT Brasil Consultoria Informática Ltda.

## AUTORES

* **Rubens Lobo**
* **Leila Farias**
* **Eduardo Cabral**
