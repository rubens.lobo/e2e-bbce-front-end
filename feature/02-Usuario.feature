# language: pt
# charset: UTF-8
Funcionalidade: Usuário
  Eu como dono de uma empresa
  Gostaria de cadastrar um novo usuário para atuar no portal

  @dev @usuario
  Cenario: CT001 - BBCE - Criar Usuário
    Dado Eu estou na pagina de login
    Quando eu logar na aplicacao com as credenciais
      |EMAIL                |SENHA      |LIMPA EMAIL|
      |leandro.leis@gft.com |Rbs@470203 |true       |
    Então sera apresentada tela do menu principal
    Quando eu clico no item do menu "Cadastro de usuários"
    E eu clico no botao novo Usuario Sistema
    E eu cadastro um usuario no sistema
      |EMPRESA                    |EMAIL                    |
      |                           |gft.atomacao06@gmail.com |
    Entao sera apresentada a mensagem "Usuário adicionado com sucesso!"
    E o usuario recebera um email com uma senha provisoria
    Dado eu faco logoff na aplicacao
    Dado Eu estou na pagina de login
    Quando eu logar na aplicacao com as credenciais
      |EMAIL       |EMPRESA  |SENHA      |
      |            |         |           |
    Quando eu confirmar uma nova senha para o usuario
      |SENHA      |
      |           |
    E criar a assinatura eletronica
      |ASSINATURA |
      |B578@Sbr9! |
    E validar token gerado
    Entao sera apresentada a mensagem "Operação realizada com sucesso !" na tela assinados