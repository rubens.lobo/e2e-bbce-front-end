# language: pt
# charset: UTF-8
Funcionalidade: Negociação
  Eu como usuário de uma empresa gostaria de comprar um titulo e outra
  empresa vende o titulo baseado na oferta feita.

  @dev @negociacao
  Cenario: CT001 - BBCE - Negociação de energia compra e venda
    Dado Eu estou na pagina de login
    Quando eu logar na aplicacao com as credenciais
      |EMAIL                      |EMPRESA                     |SENHA      |
      |gft.atomacao06@gmail.com   |Samuel Smith’s Imperial IPA |awXVNRp$   |
    Entao sera apresentada tela do menu principal
    Quando eu escolher um produto
      |TIPO |PRODUTO                              |
      |Todos|SE CON BIM OUT/19 NOV/19 PREÇO FIXO  |
    E aplicar valor de oferta
      |TIPO   |VOLUME|VALOR |
      |Comprar|0,1   |600,00|
    E confirmo com assinatura eletronica
      |ASSINATURA |
      |B578@Sbr9! |
    Entao sera apresentado a confirmacao da negociacao
    Dado eu faco logoff na aplicacao
    Dado Eu estou na pagina de login
    Quando eu logar na aplicacao com as credenciais
      |EMAIL                      |EMPRESA                  |SENHA      |
      |gft.atomacao06@gmail.com   |Founders Breakfast Stout |wS7afnYN   |
    Entao sera apresentada tela do menu principal
    Quando eu escolher um produto
      |TIPO     |
      |Ofertados|
    E aplicar valor de oferta
      |TIPO  |
      |Vender|
    E confirmo com assinatura eletronica
      |ASSINATURA |
      |B578@Sbr9! |
    Entao sera apresentado a confirmacao da negociacao de Venda


