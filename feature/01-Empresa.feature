# language: pt
# charset: UTF-8
Funcionalidade: Empresa
  Eu como admin de uma conta
  Gostaria de cadastrar uma nova empresa para atuar no portal

  @dev @empresa
  Cenario: CT001 - BBCE - Criar empresa
    Dado Eu estou na pagina de login
    Quando eu logar na aplicacao com as credenciais
      | EMAIL                   | SENHA    |
      |leandro.leis@gft.com     |Rbs@470203|
    Então sera apresentada tela do menu principal
    Quando eu clico no item do menu "Cadastro de empresas"
    Então exibe o botao "Nova empresa"
    Dado que a Nova empresa foi cadastrada com sucesso
      | TIPO EMPRESA | CATEGORIA DO AGENTE | TIPO DE ENERGIA    | TIPO DE ACORDO |
      | Sócio        | Consultoria         | Produto Financeiro | Preço fixo     |
    Quando altera o status da empresa para "ativo"
    Então o sistema exibe a empresa com status "ativo"

